package com.company.graph2;

import java.awt.*;
import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        GraphBuilder myGraphBuilder = new GraphBuilder();
        return (HashMap<Integer, Integer>) myGraphBuilder.goDijkstra(adjacencyMatrix, startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        GraphBuilder myGraphBuilder = new GraphBuilder();
        return myGraphBuilder.goPrima(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        GraphBuilder myGraphBuilder = new GraphBuilder();
        return myGraphBuilder.goKruskal(adjacencyMatrix);
    }
}
