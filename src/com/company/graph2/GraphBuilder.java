package com.company.graph2;

import java.util.*;

public class GraphBuilder {

    public Map<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int startIndex) {
        HashSet<Vertex> passedVertexes = new HashSet<>();
        HashMap<Integer, Integer> resultMap = new HashMap<>();
        resultMap.put(startIndex, 0);
        int lengthWayNow = 0;
        int indexNow = startIndex;

        while (passedVertexes.size() != adjacencyMatrix.length) {
            ArrayDeque<Vertex> vertexesInQueue = new ArrayDeque<>();
            vertexesInQueue.offerLast(new Vertex(indexNow));

            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[indexNow][j] != 0) {
                    Vertex vertex = new Vertex(j);

                    if (checkEqualVertexes(passedVertexes, vertex)) {
                        vertexesInQueue.offerLast(vertex);
                    }
                }
            }

            passedVertexes.add(vertexesInQueue.pollFirst());
            int vertexesInQueueSize = vertexesInQueue.size();
            int hypotheticalLengthWayNow = 0;

            for (int k = 0; k < vertexesInQueueSize; k++) {
                Vertex vertex = vertexesInQueue.pollFirst();

                if (!resultMap.containsKey(vertex.getNumber())) {
                    resultMap.put(vertex.getNumber(), lengthWayNow + adjacencyMatrix[indexNow][vertex.getNumber()]);
                } else {
                    if (resultMap.get(vertex.getNumber()) > lengthWayNow + adjacencyMatrix[indexNow][vertex.getNumber()]) {
                        resultMap.put(vertex.getNumber(), lengthWayNow + adjacencyMatrix[indexNow][vertex.getNumber()]);
                    }
                }

                hypotheticalLengthWayNow = lengthWayNow + adjacencyMatrix[indexNow][vertex.getNumber()];
            }

            for (int key : resultMap.keySet()) {
                if (checkEqualVertexes(passedVertexes, new Vertex(key))) {
                    if (resultMap.get(key) <= hypotheticalLengthWayNow) {
                        hypotheticalLengthWayNow = resultMap.get(key);
                        indexNow = key;
                    }
                }
            }

            lengthWayNow = hypotheticalLengthWayNow;
        }

        return resultMap;
    }

    public int goPrima(int[][] adjacencyMatrix) {
        HashSet<Vertex> passedVertexes = new HashSet<>();
        int lengthWayNow = 0;
        int indexNow = 0;

        while (passedVertexes.size() < adjacencyMatrix.length) {
            ArrayDeque<Vertex> vertexesInQueue = new ArrayDeque<>();
            vertexesInQueue.offerLast(new Vertex(indexNow));

            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[indexNow][j] != 0) {
                    Vertex vertex = new Vertex(j);

                    if (checkEqualVertexes(passedVertexes, vertex)) {
                        vertexesInQueue.offerLast(vertex);
                    }
                }
            }

            passedVertexes.add(vertexesInQueue.pollFirst());
            int hypotheticalLengthWayNow;
            int hypotheticalIndexNow;

            if (vertexesInQueue.size() != 0) {
                hypotheticalLengthWayNow = adjacencyMatrix[indexNow][vertexesInQueue.peekFirst().getNumber()];
                hypotheticalIndexNow = vertexesInQueue.peekFirst().getNumber();
            } else {
                hypotheticalLengthWayNow = 0;
                hypotheticalIndexNow = 0;
            }

            int vertexesInQueueSize = vertexesInQueue.size();
            for (int k = 0; k < vertexesInQueueSize; k++) {
                Vertex vertex = vertexesInQueue.pollFirst();
                if (adjacencyMatrix[indexNow][vertex.getNumber()] < hypotheticalLengthWayNow) {
                    hypotheticalLengthWayNow = adjacencyMatrix[indexNow][vertex.getNumber()];
                    hypotheticalIndexNow = vertex.getNumber();
                }
            }

            lengthWayNow += hypotheticalLengthWayNow;
            passedVertexes.add(new Vertex(hypotheticalIndexNow));

            int way = Integer.MAX_VALUE;

            for (Vertex vertex : passedVertexes) {
                for (int i = 0; i < adjacencyMatrix.length; i++) {
                    if (adjacencyMatrix[vertex.getNumber()][i] != 0) {
                        if (adjacencyMatrix[vertex.getNumber()][i] < way &&
                                checkEqualVertexes(passedVertexes, new Vertex(i))) {
                            way = adjacencyMatrix[vertex.getNumber()][i];
                            indexNow = vertex.getNumber();
                        }
                    }
                }
            }
        }

        return lengthWayNow;
    }

    public int goKruskal(int[][] adjacencyMatrix) {
        HashMap<Integer, ArrayList<HashSet<Vertex>>> edgesMap = new HashMap<>();
        ArrayList<Integer> sortedKeys = new ArrayList<>();
        ArrayList<HashSet<Vertex>> connectivityComponents = new ArrayList<>();
        int lengthWayNow = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    Vertex vertex = new Vertex(j);
                    ArrayList<HashSet<Vertex>> arrayList;
                    HashSet<Vertex> hashSet = new HashSet<>();

                    if (!edgesMap.containsKey(adjacencyMatrix[i][j])) {
                        arrayList = new ArrayList<>();
                    } else {
                        arrayList = edgesMap.get(adjacencyMatrix[i][j]);
                    }

                    hashSet.add(vertex);
                    hashSet.add(new Vertex(i));

                    if (!arrayList.contains(hashSet)) {
                        arrayList.add(hashSet);
                        edgesMap.put(adjacencyMatrix[i][j], arrayList);
                    }
                }
            }
        }

        sortedKeys.addAll(edgesMap.keySet());
        Collections.sort(sortedKeys);

        connectivityComponents.add(edgesMap.get(sortedKeys.get(0)).get(0));
        lengthWayNow += sortedKeys.get(0);

        for (int elem : sortedKeys) {
            for (HashSet<Vertex> edge : edgesMap.get(elem)) {
                boolean isContains = false;
                ArrayList<Integer> connectivityComponentsToConnect = new ArrayList<>();

                for (HashSet<Vertex> component : connectivityComponents) {
                    HashSet<Vertex> edgeClone = (HashSet<Vertex>) edge.clone();
                    HashSet<Vertex> componentClone = (HashSet<Vertex>) component.clone();
                    componentClone.retainAll(edgeClone);

                    if (component.containsAll(edge)) {
                        isContains = true;
                        break;
                    }

                    if (componentClone.size() != 0) {
                        connectivityComponentsToConnect.add(connectivityComponents.indexOf(component));
                        isContains = true;
                    }
                }

                if (!isContains) {
                    connectivityComponents.add(edge);
                    lengthWayNow += elem;
                }

                if (connectivityComponentsToConnect.size() == 2) {
                    connectivityComponents.get(connectivityComponentsToConnect.get(0)).
                            addAll(connectivityComponents.get(connectivityComponentsToConnect.get(1)));
                    connectivityComponents.remove(connectivityComponents.get(connectivityComponentsToConnect.get(1)));
                    lengthWayNow += elem;
                }

                if (connectivityComponentsToConnect.size() == 1) {
                    connectivityComponents.get(connectivityComponentsToConnect.get(0)).addAll(edge);
                    lengthWayNow += elem;
                }
            }
        }


        return lengthWayNow;
    }

    private boolean checkEqualVertexes(Collection<Vertex> collection, Vertex vertex) {
        for (Vertex elem : collection) {
            if (elem.equals(vertex)) {
                return false;
            }
        }

        return true;
    }
}
